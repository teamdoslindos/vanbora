<div align="center"><img src="/logo.png" alt="Project logo"></div>

Trabalho desenvolvido como parte da matéria _Tópicos Avançados em Banco de Dados_, do professor Eduardo Sakaue, sendo parte do _Curso Superior de Tecnologia em Banco de Dados_ da Fatec de São José dos Campos. Este trabalho é parte de um maior projeto da Fatec de inovar como a forma de ensino através do desenvolvimento de diversos grandes projetos no decorrer do curso.

### Objetivo

O objetivo deste projeto é desenvolver um aplicativo _mobile_ para auxiliar o concilio de viagens entre motoristas e passageiros de vans. Ou seja, o aplicativo cuidará da criação de grupos para viagens, assim como a confirmação da presença de cada passageiro, horários de saída e chegada e pontos de encontro. O aplicativo também oferece sugestão de rota ao motorista.

### Organização

O projeto está distribuído em três repositórios. Este, [Vanbora](https://gitlab.com/teamdoslindos/vanbora), serve como um hub para issues, assim como de fachada para o projeto. O repositório [Vanbora Back-end](https://gitlab.com/teamdoslindos/vanbora-back-end) contém o backend e o repositório  [Vanbora Front-end](https://gitlab.com/teamdoslindos/vanbora-front-end). Para um overview das tecnologias utilizadas, veja a próxima seção.

O desenvolvimento do projeto foi dividido em 6 sprints, cada uma com um foco específico. A tabela abaixo contém os objetivos, issues e milestones referentes a cada sprint.

| | Objetivo | Duração | Backend issues | Frontend issues |
|:-:|-|:-:|-|-|
| [**Sprint 1**](https://gitlab.com/teamdoslindos/vanbora/-/milestones/1) | Desenvolvimento do protótipo do aplicativo | 07/09–⁠20/09 | [#2][2], [#3][3], [#4][4], [#7][7] | [#1][1], [#5][5], [#6][6], [#9][9], [#11][11], [#12][12] |
| [**Sprint 2**](https://gitlab.com/teamdoslindos/vanbora/-/milestones/8) | Visualização do mapa para o usuário | 21/09–04/10 | [#17][17], [#18][18], [#19][19], [#20][20], [#21][21], [#22][22], [#23][23], [#25][25] | [#15][15], [#16][16], [#26][26] |
| [**Sprint 3**](https://gitlab.com/teamdoslindos/vanbora/-/milestones/3) | Cálculo parcial de rota | 05/10–18/10 | [#27][27] | [#28][28], [#29][29], [#30][30], [#31][31], [#32][32], [#33][33] |
| [**Sprint 4**](https://gitlab.com/teamdoslindos/vanbora/-/milestones/4) | Finalizar do cálculo de rota | 19/10–01/11 | [#34][34], [#39][39], [#42][42] | [#35][35], [#36][36], [#37][37], [#43][43] |
| [**Sprint 5**](https://gitlab.com/teamdoslindos/vanbora/-/milestones/5) | Cadastro de usuários, grupos e vans | 02/11–15/11 | [#39][39], [#48][48] |[#13][13], [#38][38], [#44][44], [#45][45], [#46][46], [#47][47] |
| [**Sprint 6**](https://gitlab.com/teamdoslindos/vanbora/-/milestones/6) | Login e finalização do aplicativo | 16/11–06/12 | [#49][49], [#50][50] | [#40][40], [#41][41], [#51][51], [#52][52], [#53][53], [#54][54] | 

### Entregas realizadas

#### Sprint 1

Entrega de protótipo de design do App, com a maior parte das telas montadas, além de ambiente de back-end configurado e pronto para fazer requisições simples, como cadastro de usuários. DevOps no Gitlab CI com build no back-end e front-end.

#### Sprint 2

Entrega da primeira versão do aplicativo, com visualização simples de mapa com a localização atual. Adição de funções do back-end como cadastros de viagens, adição de passageiro em viagens e cadastro de veículos, e adicionado deploy no [Heroku](https://vanbora-back-end.herokuapp.com/). No front-end, foi alterado a tecnologia para Java Android, assim como o pipeline de build ajustado de acordo.

#### Sprint 3

Entrega de novas funções e páginas no aplicativo, incluindo a visualização de uma rota de viagem simples em mapa e pesquisa de endereço utilizando dados reais, também a página inicial do Passageiro em que é mostrado as viagens que participa. No back-end, foi adicionada a funcionalidade de ordenação de rotas de viagens utilizando dados geográficos.

#### Sprint 4

Entrega no front-end de funções como inserção de passageiro em viagens, visualização de viagem e passageiros, alteração manual do horário estimado de cada passageiro de acordo com a experiência própria do motorista. No back-end foi implementado a ordenação de passageiros de acordo com a melhor rota.

#### Sprint 5

Entrega do back-end de ativar a função de viagem atual e otimização de persistência da ordem de passageiros na viagem. No front-end, foram feitos os Cadastros de Motorista, Viagem, Passageiro, todos com informações completas de endereço fornecidas pelo [TomTom API](https://developer.tomtom.com/), também, a finalização da tela inicial do Motorista.

#### Sprint 6

Entrega do back-end de cálculo automático de tempo entre os passageiros e login básico. No front-end, foram refatorados o fluxo entre as telas do aplicativo, login e integração das informações do usuário com as telas, mostrar mais informações da viagem de acordo com as alterações do back-end, já mostrando os horários otimizados e inseridos pelo motorista. Também foi incluído do front-end a opção do passageiro confirmar/desconfirmar a ida e volta da viagem ativa, e a opção do motorista de ativar a viagem, além de poder filtrar os passageiros confirmados. E por fim, foi feita uma análise de qualidade do app, corrigindo quaisquer pendências/bugs da aplicação.

### Estratégia

O desenvolvimento é quebrado em várias issues e cada membro do time fica responsável por uma ou mais issues por sprint. As issues devem conter a menor quantidade de interdependência o possível. O time utiliza o Git em seu máximo potencial: uma branch é criada para cada issue, issues completas são mergeadas na branch develop que, finalmente, é mergeada na branch master ao final de cada sprint. Nunca ocorre nenhum merge sem que pelo menos dois outros membros do time tenham feito code review das novas funcionalidades.

### Time

<div align="center"><table><tr><td width="400">
  </div></a>
  <a href="https://gitlab.com/daniellygj"><div align="right">
  <b>Danielly Garcia</b></a>
  &nbsp; &nbsp; &nbsp;
  <img width="75" src="/imgs/danielly.png" alt="Project logo">
  </div></a>
  <a href="https://gitlab.com/fabioromeiro"><div align="left">
  <img width="75" src="/imgs/fabio.png" alt="Project logo">
  &nbsp; &nbsp; &nbsp;
  <b>Fábio Romeiro</b> &mdash;
  Product Owner
  </div></a>
  <a href="https://gitlab.com/JeanLPierro"><div align="right">
  <b>Jean Pierro</b>
  &nbsp; &nbsp; &nbsp;
  <img width="75" src="/imgs/jean.png" alt="Project logo">
  </div></a>
  <a href="https://gitlab.com/jesscahelen"><div align="left">
  <img width="75" src="/imgs/jessica.png" alt="Project logo">
  &nbsp; &nbsp; &nbsp;
  <b>Jéssica Rosado</b> &mdash;
  Scrum Master
  </div></a>
  <a href="https://gitlab.com/marcelofsteixeira"><div align="right">
  <b>Marcelo Teixeira</b>
  &nbsp; &nbsp; &nbsp;
  <img width="75" src="/imgs/marcelo.png" alt="Project logo">
  </div></a>
  <a href="https://gitlab.com/Mateusmsouza"><div align="left">
  <img width="75" src="/imgs/mateus.png" alt="Project logo">
  &nbsp; &nbsp; &nbsp;
  <b>Mateus Machado</b>
  </div></a>
  <a href="https://github.com/pedrominicz"><div align="right">
  <b>Pedro Minicz</b>
  &nbsp; &nbsp; &nbsp;
  <img width="75" src="/imgs/pedro.png" alt="Project logo">
  </div></a>
  <a href="https://gitlab.com/RodrigoPradoDaSilva"><div align="left">
  <img width="75" src="/imgs/rodrigo.png" alt="Project logo">
  &nbsp; &nbsp; &nbsp;
  <b>Rodrigo Prado</b>
  </div></a>
</td></tr></table></div>



<!-- Links para issues, incluindo issues não existentes -->

[1]: https://gitlab.com/teamdoslindos/vanbora/-/issues/1
[2]: https://gitlab.com/teamdoslindos/vanbora/-/issues/2
[3]: https://gitlab.com/teamdoslindos/vanbora/-/issues/3
[4]: https://gitlab.com/teamdoslindos/vanbora/-/issues/4
[5]: https://gitlab.com/teamdoslindos/vanbora/-/issues/5
[6]: https://gitlab.com/teamdoslindos/vanbora/-/issues/6
[7]: https://gitlab.com/teamdoslindos/vanbora/-/issues/7
[8]: https://gitlab.com/teamdoslindos/vanbora/-/issues/8
[9]: https://gitlab.com/teamdoslindos/vanbora/-/issues/9
[10]: https://gitlab.com/teamdoslindos/vanbora/-/issues/10
[11]: https://gitlab.com/teamdoslindos/vanbora/-/issues/11
[12]: https://gitlab.com/teamdoslindos/vanbora/-/issues/12
[13]: https://gitlab.com/teamdoslindos/vanbora/-/issues/13
[14]: https://gitlab.com/teamdoslindos/vanbora/-/issues/14
[15]: https://gitlab.com/teamdoslindos/vanbora/-/issues/15
[16]: https://gitlab.com/teamdoslindos/vanbora/-/issues/16
[17]: https://gitlab.com/teamdoslindos/vanbora/-/issues/17
[18]: https://gitlab.com/teamdoslindos/vanbora/-/issues/18
[19]: https://gitlab.com/teamdoslindos/vanbora/-/issues/19
[20]: https://gitlab.com/teamdoslindos/vanbora/-/issues/20
[21]: https://gitlab.com/teamdoslindos/vanbora/-/issues/21
[22]: https://gitlab.com/teamdoslindos/vanbora/-/issues/22
[23]: https://gitlab.com/teamdoslindos/vanbora/-/issues/23
[24]: https://gitlab.com/teamdoslindos/vanbora/-/issues/24
[25]: https://gitlab.com/teamdoslindos/vanbora/-/issues/25
[26]: https://gitlab.com/teamdoslindos/vanbora/-/issues/26
[27]: https://gitlab.com/teamdoslindos/vanbora/-/issues/27
[28]: https://gitlab.com/teamdoslindos/vanbora/-/issues/28
[29]: https://gitlab.com/teamdoslindos/vanbora/-/issues/29
[30]: https://gitlab.com/teamdoslindos/vanbora/-/issues/30
[31]: https://gitlab.com/teamdoslindos/vanbora/-/issues/31
[32]: https://gitlab.com/teamdoslindos/vanbora/-/issues/32
[33]: https://gitlab.com/teamdoslindos/vanbora/-/issues/33
[34]: https://gitlab.com/teamdoslindos/vanbora/-/issues/34
[35]: https://gitlab.com/teamdoslindos/vanbora/-/issues/35
[36]: https://gitlab.com/teamdoslindos/vanbora/-/issues/36
[37]: https://gitlab.com/teamdoslindos/vanbora/-/issues/37
[38]: https://gitlab.com/teamdoslindos/vanbora/-/issues/38
[39]: https://gitlab.com/teamdoslindos/vanbora/-/issues/39
[40]: https://gitlab.com/teamdoslindos/vanbora/-/issues/40
[41]: https://gitlab.com/teamdoslindos/vanbora/-/issues/41
[42]: https://gitlab.com/teamdoslindos/vanbora/-/issues/42
[43]: https://gitlab.com/teamdoslindos/vanbora/-/issues/43
[44]: https://gitlab.com/teamdoslindos/vanbora/-/issues/44
[45]: https://gitlab.com/teamdoslindos/vanbora/-/issues/45
[46]: https://gitlab.com/teamdoslindos/vanbora/-/issues/46
[47]: https://gitlab.com/teamdoslindos/vanbora/-/issues/47
[48]: https://gitlab.com/teamdoslindos/vanbora/-/issues/48
[49]: https://gitlab.com/teamdoslindos/vanbora/-/issues/49
[50]: https://gitlab.com/teamdoslindos/vanbora/-/issues/50
[51]: https://gitlab.com/teamdoslindos/vanbora/-/issues/51
[52]: https://gitlab.com/teamdoslindos/vanbora/-/issues/52
[53]: https://gitlab.com/teamdoslindos/vanbora/-/issues/53
[54]: https://gitlab.com/teamdoslindos/vanbora/-/issues/54